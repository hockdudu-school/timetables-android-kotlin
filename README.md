# Timetables
## Info

Timetables is an Android application which aims helping students to find the correct classroom.

Example: I study in a big school with lots of classrooms, and every lesson was on another room. I simply didn't manage to memorize when and on which room I should be.

That's why I wrote this app. It shows you when, and, more importantly, where you should be.

The information can either be downloaded from the internet or manually given as a simple JSON.

## Motivation
### Why write a new app if there are others apps for that, e.g. the school has one?

Simply put, the school's app is awful. It shows too much unnecessary information and doesn't support lesson merging (that is, automatically merging two different classes into one). Moreover, it is always nice to learn something new, in this case writing Android apps and the Kotlin programming language.

### Why open source?

As all my school projects, this project is also open-source. The reason is, I do this not for money or pride, but for the sake of doing something, for the sake of learning. With it being open-source, not only I am the one who learns something, but anyone can see how it was done, anyone can learn too.

I hope I motivate others into also doing the same. It may require a lot of time and patience, but, at the end, you're going to be a better developer.

And I can also become a better developer if this application is open-source. If anyone finds an error or a potential improvement, they can be added to the project and code too, sharing tips and such.

### Why dark theme?

<details>
<summary>Simply because I prefer dark themes!</summary>

<img src="https://i.imgur.com/jObeP1r.jpg" />
<br>
<sup>Not so professional of me, I know</sup>

</details>

## Installation

Simply click on "Tag (x)" above and download the latest version!