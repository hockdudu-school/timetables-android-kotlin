package ch.hockdudu.timetables

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import ch.hockdudu.timetables.lesson.Lesson
import ch.hockdudu.timetables.lesson.LessonAdapter
import ch.hockdudu.timetables.lesson.LessonRoomDatabase
import ch.hockdudu.timetables.utils.Constants.Companion.ACTIVITY_RESULT_LESSON_UPDATE
import ch.hockdudu.timetables.utils.Constants.Companion.EXTRA_LESSON
import ch.hockdudu.timetables.utils.Constants.Companion.INTENT_FILTER_REFRESH_FROM_DB
import ch.hockdudu.timetables.utils.Constants.Companion.INTENT_FILTER_REFRESH_ITEMS
import ch.hockdudu.timetables.utils.Constants.Companion.SAVED_INSTANCE_SHOULD_RELOAD_LIST
import ch.hockdudu.timetables.utils.separateLessonByDate

class MainActivity : AppCompatActivity() {
    private lateinit var itemsHolder: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private val lessonItemsList = mutableListOf<Any>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(findViewById(R.id.toolbar))

        itemsHolder = findViewById(R.id.lesson_view)
        itemsHolder.layoutManager = LinearLayoutManager(this)
        itemsHolder.adapter = LessonAdapter(lessonItemsList, this)
        itemsHolder.itemAnimator = DefaultItemAnimator()

        swipeRefreshLayout = findViewById(R.id.swipe_refresh)

        swipeRefreshLayout.setOnRefreshListener {
            refresh()
        }

        if (savedInstanceState == null) {
            swipeRefreshLayout.isRefreshing = true
            updateWeekLessons()
        } else if (savedInstanceState.getBoolean(SAVED_INSTANCE_SHOULD_RELOAD_LIST)) {
            updateWeekLessons()
        }

        val filter = IntentFilter()
        filter.addAction(INTENT_FILTER_REFRESH_ITEMS)
        filter.addAction(INTENT_FILTER_REFRESH_FROM_DB)

        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastListener, filter)
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastListener)
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.refresh -> {
                swipeRefreshLayout.isRefreshing = true
                refresh()

                true
            }
            R.id.next_lesson -> {
                if (lessonItemsList.isNotEmpty()) {
                    val intent = Intent(this, LessonInfoActivity::class.java)
                    startActivity(intent)
                } else {
                    val toast = Toast.makeText(this, R.string.error_no_lesson_found, Toast.LENGTH_SHORT)
                    toast.show()
                }

                true
            }
            R.id.backup -> {
                val intent = Intent(this, BackupActivity::class.java)
                startActivity(intent)
                true
            }
            R.id.info -> {
                val intent = Intent(this, AboutActivity::class.java)
                startActivity(intent)
                true
            }
            R.id.new_lesson -> {
                val intent = Intent(this, LessonEditActivity::class.java)
                startActivityForResult(intent, ACTIVITY_RESULT_LESSON_UPDATE)
                true
            }
            else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            ACTIVITY_RESULT_LESSON_UPDATE -> {
                if (resultCode == Activity.RESULT_OK) {
                    val lesson = data!!.getParcelableExtra<Lesson>(EXTRA_LESSON)

                    val existingLesson = lessonItemsList.asSequence().filterIsInstance(Lesson::class.java).firstOrNull {
                        it.id == lesson.id
                    }

                    if (existingLesson != null) {
                        val existingLessonIndex = lessonItemsList.indexOf(existingLesson)

                        lessonItemsList.removeAt(existingLessonIndex)
                    }

                    lessonItemsList.add(lesson)
                    onUpdatedLessons(lessonItemsList.filterIsInstance(Lesson::class.java))
                }
            }
        }
    }

    private fun updateWeekLessons() {
        val handler = Handler()
        val runnable = Runnable {
            val lessonDao = LessonRoomDatabase.getDatabase(application).lessonDao()
            val allLessons = lessonDao.getAllLessons()

            handler.post {
                onUpdatedLessons(allLessons)
            }
        }

        Thread(runnable).start()
    }

    private fun onUpdatedLessons(lessonList: List<Lesson>?) {
        lessonItemsList.clear()

        if (lessonList != null && lessonList.isNotEmpty()) {
            (lessonList as MutableList).sort()
            val lessonsByDate = separateLessonByDate(lessonList)

            lessonsByDate.forEach {
                lessonItemsList.add(it.key)
                it.value.forEach { lesson ->
                    lessonItemsList.add(lesson)
                }
            }
        }

        itemsHolder.adapter.notifyDataSetChanged()
        swipeRefreshLayout.isRefreshing = false
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.run {
            putBoolean(SAVED_INSTANCE_SHOULD_RELOAD_LIST, lessonItemsList.count() > 0)
        }
        super.onSaveInstanceState(outState)
    }

    private fun refresh() {
        lessonItemsList.clear()
        itemsHolder.adapter.notifyDataSetChanged()
        updateWeekLessons()
    }

    private val broadcastListener = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            if (intent != null) {
                if (IntentFilter(INTENT_FILTER_REFRESH_FROM_DB).hasAction(intent.action)) refresh()
                if (IntentFilter(INTENT_FILTER_REFRESH_ITEMS).hasAction(intent.action)) onUpdatedLessons(lessonItemsList.filterIsInstance(Lesson::class.java))
            }
        }
    }
}