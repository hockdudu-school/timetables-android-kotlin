package ch.hockdudu.timetables

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Handler
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import ch.hockdudu.timetables.lesson.Lesson
import ch.hockdudu.timetables.lesson.LessonRoomDatabase
import ch.hockdudu.timetables.lesson.nextLesson
import ch.hockdudu.timetables.utils.Constants.Companion.EXTRA_LESSON
import ch.hockdudu.timetables.utils.convertLessonTimeToDate
import ch.hockdudu.timetables.utils.getAppropriateMondayFromLessonList
import java.lang.IllegalArgumentException
import java.text.DateFormat
import java.util.*

class LessonInfoActivity : AppCompatActivity() {

    private val timeChangeIntentFilter = IntentFilter(Intent.ACTION_TIME_TICK)
    private lateinit var progressBar: ProgressBar
    private var lessons: List<Lesson> = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lesson_info)

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.lesson)

        progressBar = findViewById(R.id.progress_bar)

        if (intent.hasExtra(EXTRA_LESSON)) {
            val lesson = intent.getParcelableExtra<Lesson>(EXTRA_LESSON)
            lessons = List(1) { lesson }

            updateView()
            registerReceiver(timeChangeBroadcastListener, timeChangeIntentFilter)
        } else {
            val handler = Handler()
            val runnable = Runnable {
                val lessonDao = LessonRoomDatabase.getDatabase(this).lessonDao()
                lessons = lessonDao.getAllLessons()

                handler.post {
                    if (lessons.isNotEmpty()) {
                        updateView()
                        registerReceiver(timeChangeBroadcastListener, timeChangeIntentFilter)
                    } else {
                        Toast.makeText(this@LessonInfoActivity, R.string.error_no_lesson_found, Toast.LENGTH_SHORT).show()
                        finish()
                    }
                }
            }
            Thread(runnable).start()
        }
    }

    override fun onResume() {
        super.onResume()
        updateView()
    }

    override fun onDestroy() {
        try {
            unregisterReceiver(timeChangeBroadcastListener)
        } catch (_: IllegalArgumentException) {}
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun updateView() {
        val mondayCalendar = getAppropriateMondayFromLessonList(lessons)
        val lesson = nextLesson(lessons, mondayCalendar) ?: return
        setTextContent(lesson, mondayCalendar)
        updateProgressBar(lesson, mondayCalendar)
    }

    private fun setTextContent(lesson: Lesson, mondayCalendar: Calendar) {
        val dateFormat = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, Locale.getDefault())

        this.findViewById<TextView>(R.id.start)?.text = dateFormat.format(convertLessonTimeToDate(lesson.start, lesson.dayOfWeek, mondayCalendar))
        this.findViewById<TextView>(R.id.end)?.text = dateFormat.format(convertLessonTimeToDate(lesson.end, lesson.dayOfWeek, mondayCalendar))
        this.findViewById<TextView>(R.id.location)?.text = lesson.location
        this.findViewById<TextView>(R.id.teacher)?.text = lesson.teacher
        this.findViewById<TextView>(R.id.summary)?.text = lesson.summary
    }

    private fun updateProgressBar(lesson: Lesson, mondayCalendar: Calendar) {
        val now = Calendar.getInstance().timeInMillis
        val lessonStart = convertLessonTimeToDate(lesson.start, lesson.dayOfWeek, mondayCalendar).time
        val lessonEnd = convertLessonTimeToDate(lesson.end, lesson.dayOfWeek, mondayCalendar).time

        if (now in lessonStart..lessonEnd) {
            val rangeSize = lessonEnd - lessonStart
            val relativeNow = now - lessonStart

            progressBar.max = rangeSize.toInt()
            progressBar.progress = relativeNow.toInt()

            setProgressbarVisibility(true)
        } else setProgressbarVisibility(false)
    }

    private val timeChangeBroadcastListener = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent?) {
            updateView()
        }
    }

    private fun setProgressbarVisibility(visible: Boolean) {
        val dividerLayout = findViewById<View>(R.id.divider_time_summary).layoutParams as ConstraintLayout.LayoutParams

        if (visible) {
            progressBar.visibility = ProgressBar.VISIBLE
            dividerLayout.topToBottom = R.id.progress_bar
        } else {
            progressBar.visibility = ProgressBar.INVISIBLE
            dividerLayout.topToBottom = R.id.barrier_time_progressbar
        }
    }
}