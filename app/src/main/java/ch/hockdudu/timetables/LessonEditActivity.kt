package ch.hockdudu.timetables

import android.app.Activity
import android.app.TimePickerDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.TextInputLayout
import android.text.format.DateFormat
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import ch.hockdudu.timetables.lesson.Lesson
import ch.hockdudu.timetables.lesson.LessonRoomDatabase
import ch.hockdudu.timetables.lesson.LessonTime
import ch.hockdudu.timetables.utils.Constants.Companion.EXTRA_LESSON
import java.text.DateFormatSymbols
import java.text.ParseException
import java.util.*

class LessonEditActivity : AppCompatActivity() {

    private val dateFormat: java.text.DateFormat = java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT, Locale.getDefault())

    private lateinit var spinner: Spinner
    private lateinit var start: EditText
    private lateinit var end: EditText
    private lateinit var location: EditText
    private lateinit var summary: EditText
    private lateinit var teacher: EditText
    private lateinit var startHolder: TextInputLayout
    private lateinit var endHolder: TextInputLayout

    private var isEditingCurrentLesson = false
    private lateinit var lesson: Lesson

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_lesson)

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        val filteredWeekDays = mutableListOf<String>()
        DateFormatSymbols.getInstance().weekdays.forEach {
            if (!it.isEmpty()) filteredWeekDays.add(it)
        }

        Collections.rotate(filteredWeekDays, -1) // Show monday first

        val weekDaysAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, filteredWeekDays)

        spinner = findViewById(R.id.day_of_week_spinner)
        spinner.adapter = weekDaysAdapter

        start = findViewById(R.id.start)
        end = findViewById(R.id.end)
        location = findViewById(R.id.location)
        summary = findViewById(R.id.summary)
        teacher = findViewById(R.id.teacher)

        startHolder = findViewById(R.id.start_holder)
        endHolder = findViewById(R.id.end_holder)


        val startButton = findViewById<Button>(R.id.start_button)
        val endButton = findViewById<Button>(R.id.end_button)

        startButton.setOnClickListener {
            val parsedTime = parseUserTimeInput(start.text.toString())

            TimePickerDialog(this, { _: TimePicker?, hourOfDay: Int, minute: Int ->
                run {
                    setEditTextBasedOnTime(start, hourOfDay, minute)
                    timePickerFocusChangeListener(start, startHolder, start.hasFocus())
                }
            }, parsedTime?.hour ?: 12, parsedTime?.minute ?: 0,
                    DateFormat.is24HourFormat(this)).show()
        }

        endButton.setOnClickListener {
            val parsedTime = parseUserTimeInput(end.text.toString())

            TimePickerDialog(this, { _: TimePicker?, hourOfDay: Int, minute: Int ->
                run {
                    setEditTextBasedOnTime(end, hourOfDay, minute)
                    timePickerFocusChangeListener(end, endHolder, end.hasFocus())
                }
            }, parsedTime?.hour ?: 12, parsedTime?.minute ?: 0,
                    DateFormat.is24HourFormat(this)).show()
        }

        start.setOnFocusChangeListener { _, hasFocus -> timePickerFocusChangeListener(start, startHolder, hasFocus) }
        end.setOnFocusChangeListener { _, hasFocus -> timePickerFocusChangeListener(end, endHolder, hasFocus) }

        if (intent.hasExtra(EXTRA_LESSON)) {
            isEditingCurrentLesson = true

            lesson = intent.getParcelableExtra(EXTRA_LESSON)

            setEditTextBasedOnTime(start, lesson.start.hour, lesson.start.minute)
            setEditTextBasedOnTime(end, lesson.end.hour, lesson.end.minute)
            location.setText(lesson.location)
            summary.setText(lesson.summary)
            teacher.setText(lesson.teacher)

            // -1 because days of week array is shifted to start on monday,
            // -1 again because days of week from Calendar are on the 1-7 range instead of 0-6
            spinner.setSelection(lesson.dayOfWeek - 2)

            supportActionBar!!.title = getString(R.string.edit_edit_lesson)
        } else {
            supportActionBar!!.title = getString(R.string.edit_new_lesson)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.edit_lesson_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            val resultIntent = Intent()
            setResult(Activity.RESULT_CANCELED, resultIntent)
            finish()
            true
        }
        R.id.save -> {
            var parsedStart = parseUserTimeInput(start.text.toString())
            var parsedEnd = parseUserTimeInput(end.text.toString())

            if (parsedStart != null && parsedEnd != null && parsedStart > parsedEnd) {
                // Swap values
                val temp = parsedStart
                parsedStart = parsedEnd
                parsedEnd = temp
            }

            if (parsedStart != null && parsedEnd != null) {

                val handler = Handler()

                val runnable = Runnable {
                    val lessonDao = LessonRoomDatabase.getDatabase(application).lessonDao()

                    if (!isEditingCurrentLesson) lesson = Lesson()

                    lesson.start = parsedStart
                    lesson.end = parsedEnd
                    lesson.location = location.text.toString()
                    lesson.summary = summary.text.toString()
                    lesson.teacher = teacher.text.toString()

                    // +1 because days of week array is shifted to start on monday,
                    // +1 again because days of week from Calendar are on the 1-7 range instead of 0-6
                    lesson.dayOfWeek = spinner.selectedItemPosition + 2

                    if (isEditingCurrentLesson) {
                        lessonDao.update(lesson)
                    } else {
                        lessonDao.insert(lesson)
                    }

                    handler.post {
                        val resultIntent = Intent()
                        resultIntent.putExtra(EXTRA_LESSON, lesson)
                        setResult(Activity.RESULT_OK, resultIntent)
                        finish()
                    }
                }

                Thread(runnable).start()

            } else {
                val toast = Toast.makeText(this, R.string.edit_lesson_enter_valid_time, Toast.LENGTH_SHORT)
                toast.show()
            }

            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun setEditTextBasedOnTime(editText: EditText, hourOfDay: Int, minute: Int) {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
        calendar.set(Calendar.MINUTE, minute)

        editText.setText(dateFormat.format(calendar.time))
    }

    private fun timePickerFocusChangeListener(editText: EditText, editTextInputLayout: TextInputLayout, hasFocus: Boolean) {
        if (!hasFocus) {
            val editTextText = editText.text.toString()

            val isDateValid = try {
                dateFormat.parse(editTextText)
                true
            } catch (e: ParseException) {
                false
            }

            if (isDateValid || editTextText.isEmpty()) {
                editTextInputLayout.isErrorEnabled = false
            } else {
                editTextInputLayout.isErrorEnabled = true
                editTextInputLayout.error = getString(R.string.edit_lesson_enter_valid_time)
            }
        }
    }

    private fun parseUserTimeInput(userInput: String): LessonTime? {
        return try {
            val parsedCalendar = Calendar.getInstance()
            parsedCalendar.time = dateFormat.parse(userInput)
            val lessonTime = LessonTime()
            lessonTime.hour = parsedCalendar.get(Calendar.HOUR_OF_DAY)
            lessonTime.minute = parsedCalendar.get(Calendar.MINUTE)
            return lessonTime
        } catch (e: ParseException) {
            null
        }
    }
}
