package ch.hockdudu.timetables.lesson

import android.arch.persistence.room.TypeConverter
import java.lang.NumberFormatException
import java.util.*

/**
 * Holds the values [hour] and [minute], with the advantage as being comparable, the ability of
 * converting to an Integer ([toInt])
 */
class LessonTime() : Comparable<LessonTime> {

    constructor(timeAsInt: Int) : this() {
        val parsed = toLessonTime(timeAsInt)
        this.hour = parsed.hour
        this.minute = parsed.minute
    }

    /**
     * @constructor
     *
     * Sets the hour and minutes with the given [String] in the format "hh:mm"
     */
    constructor(timeAsString: String) : this() {
        val splitString = timeAsString.split(':', limit = 2)

        if (splitString.count() == 2) {
            try {
                val hourString = splitString[0]
                val minuteString = splitString[1]
                val hour = Integer.parseInt(hourString)
                val minute = Integer.parseInt(minuteString)

                this.hour = hour
                this.minute = minute
            } catch (_: NumberFormatException) {
            }
        }
    }

    override fun compareTo(other: LessonTime): Int {
        val hourCompare = this.hour.compareTo(other.hour)

        return when {
            hourCompare != 0 -> hourCompare
            else -> this.minute.compareTo(other.minute)
        }
    }

    var hour: Int = 0
        set(value) {
            field = when (value) {
                in 0..23 -> value
                else -> 0
            }
        }

    var minute: Int = 0
        set(value) {
            field = when (value) {
                in 0..59 -> value
                else -> 0
            }
        }

    @TypeConverter
    fun toLessonTime(value: Int): LessonTime {
        val lessonTime = LessonTime()

        lessonTime.hour = value / 60
        lessonTime.minute = value % 60

        return lessonTime
    }

    @TypeConverter
    fun fromLessonTime(lessonTime: LessonTime): Int {
        return lessonTime.hour * 60 + lessonTime.minute
    }

    fun toInt(): Int {
        return fromLessonTime(this)
    }

    fun toDate(): Date {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, hour)
        calendar.set(Calendar.MINUTE, minute)
        calendar.set(Calendar.SECOND, 0)

        return calendar.time
    }

    override fun toString(): String {
        return String.format("%02d:%02d", hour, minute)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true

        if (other == null) return false

        if (other !is LessonTime) return false

        return this.hour == other.hour && this.minute == other.minute
    }

    override fun hashCode(): Int {
        return toInt()
    }
}