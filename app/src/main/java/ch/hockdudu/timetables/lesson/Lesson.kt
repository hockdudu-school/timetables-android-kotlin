package ch.hockdudu.timetables.lesson

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import android.os.Parcel
import android.os.Parcelable
import java.util.*

@Entity
class Lesson() : Comparable<Lesson>, Parcelable {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0

    @TypeConverters(LessonTime::class)
    var start: LessonTime = LessonTime()

    @TypeConverters(LessonTime::class)
    var end: LessonTime = LessonTime()

    var dayOfWeek: Int = Calendar.MONDAY
        set(value) {
            field = when (value) {
                in Calendar.SUNDAY..Calendar.SATURDAY -> value
                else -> Calendar.MONDAY
            }
        }

    var summary: String = ""

    var location: String = ""

    var teacher: String = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        start = LessonTime(parcel.readInt())
        end = LessonTime(parcel.readInt())
        dayOfWeek = parcel.readInt()
        summary = parcel.readString()
        location = parcel.readString()
        teacher = parcel.readString()
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
        dest?.writeInt(id)
        dest?.writeInt(start.toInt())
        dest?.writeInt(end.toInt())
        dest?.writeInt(dayOfWeek)
        dest?.writeString(summary)
        dest?.writeString(location)
        dest?.writeString(teacher)
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun compareTo(other: Lesson): Int {
        val weekCompare = this.dayOfWeek.compareTo(other.dayOfWeek)
        if (weekCompare != 0) return weekCompare

        val startCompare = this.start.compareTo(other.start)
        if (startCompare != 0) return startCompare

        return this.end.compareTo(other.end)
    }

    override fun equals(other: Any?): Boolean {
        if (other == null) return false

        if (other !is Lesson) return false

        return id == other.id &&
                start == other.start &&
                end == other.end &&
                dayOfWeek == other.dayOfWeek &&
                summary == other.summary &&
                location == other.location &&
                teacher == other.teacher
    }

    override fun hashCode(): Int {
        var result = 17
        result = result * 31 + id
        result = result * 31 + start.hashCode()
        result = result * 31 + end.hashCode()
        result = result * 31 + dayOfWeek
        result = result * 31 + summary.hashCode()
        result = result * 31 + location.hashCode()
        result = result * 31 + teacher.hashCode()

        return result
    }

    companion object CREATOR : Parcelable.Creator<Lesson> {
        override fun createFromParcel(parcel: Parcel): Lesson {
            return Lesson(parcel)
        }

        override fun newArray(size: Int): Array<Lesson?> {
            return arrayOfNulls(size)
        }
    }
}