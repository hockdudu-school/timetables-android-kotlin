package ch.hockdudu.timetables.lesson

import android.content.Context
import android.content.Intent
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.TextView
import ch.hockdudu.timetables.LessonInfoActivity
import ch.hockdudu.timetables.LessonEditActivity
import ch.hockdudu.timetables.R
import java.text.DateFormat
import java.text.DateFormatSymbols
import java.util.*
import android.app.Activity
import android.support.design.widget.Snackbar
import android.support.v4.content.LocalBroadcastManager
import android.widget.Toast.LENGTH_SHORT
import ch.hockdudu.timetables.utils.Constants.Companion.ACTIVITY_RESULT_LESSON_UPDATE
import ch.hockdudu.timetables.utils.Constants.Companion.EXTRA_LESSON
import ch.hockdudu.timetables.utils.Constants.Companion.INTENT_FILTER_REFRESH_ITEMS


class LessonAdapter(private val itemsList: List<Any>, private val context: Context) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val VIEW_ITEM = 0
        private const val VIEW_TITLE = 1
    }

    private var dateFormat: DateFormat = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = parent.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val viewHolder: RecyclerView.ViewHolder

        when (viewType) {
            VIEW_ITEM -> {
                val inflatedView = layoutInflater.inflate(R.layout.view_lesson_item, parent, false)
                viewHolder = ViewHolderItems(inflatedView)

                viewHolder.itemView.setOnClickListener {
                    val position = viewHolder.adapterPosition
                    val lesson = itemsList[position] as Lesson

                    val intent = Intent(context, LessonInfoActivity::class.java).apply {
                        putExtra(EXTRA_LESSON, lesson)
                    }
                    context.startActivity(intent)
                }

                viewHolder.itemView.setOnLongClickListener {
                    val position = viewHolder.adapterPosition
                    val lesson = itemsList[position] as Lesson

                    val popup = PopupMenu(context, it, Gravity.END)
                    popup.inflate(R.menu.lesson_long_press_menu)
                    popup.setOnMenuItemClickListener { menuItem: MenuItem? ->
                        when (menuItem?.itemId) {
                            R.id.edit -> {
                                val intent = Intent(context, LessonEditActivity::class.java).apply {
                                    putExtra(EXTRA_LESSON, lesson)
                                }
                                (context as Activity).startActivityForResult(intent, ACTIVITY_RESULT_LESSON_UPDATE)
                                true
                            }
                            R.id.delete -> {
                                val runnable = Runnable {
                                    val lessonDao = LessonRoomDatabase.getDatabase(context).lessonDao()
                                    val refreshIntent = Intent(INTENT_FILTER_REFRESH_ITEMS)

                                    lessonDao.delete(lesson)
                                    (itemsList as MutableList).removeAt(position)

                                    LocalBroadcastManager.getInstance(context).sendBroadcast(refreshIntent)

                                    val snackbar = Snackbar.make(parent, R.string.lesson_deleted, LENGTH_SHORT)
                                    snackbar.setAction(R.string.undo) { _ ->
                                        Thread {
                                            lessonDao.insert(lesson)
                                            itemsList.add(lesson) //Position doesn't matter, as it will be sorted again after
                                            LocalBroadcastManager.getInstance(context).sendBroadcast(refreshIntent)
                                        }.start()
                                    }
                                    snackbar.show()
                                }

                                Thread(runnable).start()
                                true
                            }
                            else -> {
                                false
                            }
                        }
                    }
                    popup.show()
                    true
                }
            }
            VIEW_TITLE -> {
                val inflatedView = layoutInflater.inflate(R.layout.view_lesson_title, parent, false)
                viewHolder = ViewHolderTitle(inflatedView)
            }
            else -> {
                throw IllegalArgumentException("Unknown viewType $viewType")
            }
        }

        return viewHolder
    }

    override fun getItemCount(): Int = itemsList.count()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder.itemViewType) {
            VIEW_ITEM -> {
                val holderItems = holder as ViewHolderItems
                val item = itemsList[position] as Lesson
                holderItems.location.text = item.location
                holderItems.summary.text = item.summary
                holderItems.teacher.text = item.teacher
                holderItems.start.text = dateFormat.format(item.start.toDate())
            }
            VIEW_TITLE -> {
                val weekdays = DateFormatSymbols.getInstance().weekdays

                val holderTitle = holder as ViewHolderTitle
                val weekday = itemsList[position] as Int

                holderTitle.title.text = weekdays[weekday]
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            itemsList[position] is Lesson -> VIEW_ITEM
            itemsList[position] is Int -> VIEW_TITLE
            else -> -1
        }
    }

    class ViewHolderItems(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var location: TextView = itemView!!.findViewById(R.id.location)
        var summary: TextView = itemView!!.findViewById(R.id.summary)
        var teacher: TextView = itemView!!.findViewById(R.id.teacher)
        var start: TextView = itemView!!.findViewById(R.id.start)
    }

    class ViewHolderTitle(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView!!.findViewById(R.id.title)
    }
}