package ch.hockdudu.timetables.lesson

import ch.hockdudu.timetables.utils.convertLessonTimeToDate
import java.util.*


/**
 * Given a Instance of [Calendar] on the Monday of the wanted week (for getting the correct time
 * from each [Lesson], return the next lesson based on the current time
 */
fun nextLesson(lessons: List<Lesson>, mondayCalendar: Calendar): Lesson? {
    return nextLesson(lessons, mondayCalendar, Calendar.getInstance().time)
}

/**
 * Given a Instance of [Calendar] on the Monday of the wanted week (for getting the correct time
 * from each [Lesson], return the next lesson based on the current time, given as [Date]
 */
fun nextLesson(lessons: List<Lesson>, mondayCalendar: Calendar, now: Date): Lesson? {
    if (lessons.isEmpty())
        return null

    (lessons as MutableList).sort()

    //Sets next lesson to be the last lesson
    var nextLesson = lessons.last()


    for (lesson in lessons) {
        val startDate = convertLessonTimeToDate(lesson.start, lesson.dayOfWeek, mondayCalendar)
        val endDate = convertLessonTimeToDate(lesson.end, lesson.dayOfWeek, mondayCalendar)

        if (now in startDate..endDate) {
            nextLesson = lesson
            break
        }

        val nextLessonStartDate = convertLessonTimeToDate(nextLesson.start, nextLesson.dayOfWeek, mondayCalendar)
        //If the lesson is after now and newer than the previous nextLesson
        if (startDate in now..nextLessonStartDate) {
            nextLesson = lesson
        }
    }

    return nextLesson
}