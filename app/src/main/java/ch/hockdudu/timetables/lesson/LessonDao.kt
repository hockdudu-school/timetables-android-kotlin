package ch.hockdudu.timetables.lesson

import android.arch.persistence.room.*

@Dao
interface LessonDao {

    @Insert
    fun insert(lesson: Lesson)

    @Query("SELECT * FROM lesson")
    fun getAllLessons(): List<Lesson>

    @Delete
    fun delete(lesson: Lesson)

    @Update
    fun update(lesson: Lesson)

    @TypeConverters(LessonTime::class)
    @Query("SELECT COUNT(*) FROM lesson WHERE start = :start AND `end` = :end AND dayOfWeek = :dayOfWeek AND summary = :summary AND location = :location AND teacher = :teacher")
    fun getEqualsCount(start: LessonTime, end: LessonTime, dayOfWeek: Int, summary: String, location: String, teacher: String): Int
}