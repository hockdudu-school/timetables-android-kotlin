package ch.hockdudu.timetables.lesson

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

@Database(entities = [Lesson::class], version = 1)
abstract class LessonRoomDatabase : RoomDatabase() {

    abstract fun lessonDao(): LessonDao

    companion object {
        private var INSTANCE: LessonRoomDatabase? = null

        fun getDatabase(context: Context): LessonRoomDatabase {
            if (INSTANCE == null) {
                synchronized(LessonRoomDatabase::class.java) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                LessonRoomDatabase::class.java, "lesson_database")
                                .build()
                    }
                }
            }

            return INSTANCE!!
        }
    }
}