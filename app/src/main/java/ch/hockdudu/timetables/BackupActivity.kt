package ch.hockdudu.timetables

import android.app.Activity
import android.content.*
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.LinearLayout
import android.os.Handler
import android.support.v4.content.LocalBroadcastManager
import ch.hockdudu.timetables.lesson.LessonRoomDatabase
import ch.hockdudu.timetables.utils.convertLessonsToJson
import android.widget.Toast
import ch.hockdudu.timetables.lesson.Lesson
import ch.hockdudu.timetables.utils.Constants
import ch.hockdudu.timetables.utils.convertJsonStringToLessonsList
import java.io.*

class BackupActivity : AppCompatActivity() {

    companion object {
        private const val BACKUP_REQUEST_CODE = 0
        private const val RESTORE_REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_backup)

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.backup_activity_name)

        val backupButton = findViewById<LinearLayout>(R.id.backup)
        val restoreButton = findViewById<LinearLayout>(R.id.restore)

        backupButton.setOnClickListener { _ ->
            val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "application/json"
                putExtra(Intent.EXTRA_TITLE, "timetables.json")
            }

            startActivityForResult(intent, BACKUP_REQUEST_CODE)
        }

        restoreButton.setOnClickListener { _ ->
            val intent = Intent(Intent.ACTION_GET_CONTENT).apply {
                addCategory(Intent.CATEGORY_OPENABLE)
                type = "application/json"
            }

            startActivityForResult(intent, RESTORE_REQUEST_CODE)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    private fun onBackupFileSelected(uri: Uri?) {
        val handler = Handler()
        val runnable = Runnable {
            val lessonDao = LessonRoomDatabase.getDatabase(this).lessonDao()
            val lessons = lessonDao.getAllLessons()
            val lessonsJson = convertLessonsToJson(lessons)

            try {
                contentResolver.openFileDescriptor(uri, "w")?.use { parcelFileDescriptor ->
                    // use{} lets the document provider know you're done by automatically closing the stream
                    FileOutputStream(parcelFileDescriptor.fileDescriptor).use { outputStream ->
                        outputStream.write(lessonsJson.toByteArray())
                    }
                }
                handler.post {
                    Toast.makeText(this, R.string.backup_backup_ok, Toast.LENGTH_SHORT).show()
                }
            } catch (_: IOException) {
                handler.post {
                    Toast.makeText(this, R.string.backup_backup_error, Toast.LENGTH_SHORT).show()
                }
            }
        }

        Thread(runnable).start()
    }

    private fun onRestoreFileSelected(uri: Uri?) {
        try {
            contentResolver.openFileDescriptor(uri, "r")?.use { parcelFileDescriptor ->
                // use{} lets the document provider know you're done by automatically closing the stream
                FileInputStream(parcelFileDescriptor.fileDescriptor).use { fileInputStream ->
                    val jsonString = String(fileInputStream.readBytes())
                    val lessons = convertJsonStringToLessonsList(jsonString)

                    if (lessons != null) {
                        restoreLessons(lessons)
                    } else {
                        Toast.makeText(this, R.string.backup_restore_error, Toast.LENGTH_SHORT).show()
                    }
                }
            }
        } catch (_: IOException) {
            Toast.makeText(this, R.string.backup_restore_error, Toast.LENGTH_SHORT).show()
        }
    }

    private fun restoreLessons(lessons: List<Lesson>) {
        val handler = Handler()
        val runnable = Runnable {
            val lessonDao = LessonRoomDatabase.getDatabase(this).lessonDao()
            lessons.forEach {
                val equalsCount = lessonDao.getEqualsCount(it.start, it.end, it.dayOfWeek, it.summary, it.location, it.teacher)
                if (equalsCount == 0) lessonDao.insert(it)
            }

            handler.post {
                Toast.makeText(this, R.string.backup_restore_ok, Toast.LENGTH_SHORT).show()

                val intent = Intent(Constants.INTENT_FILTER_REFRESH_FROM_DB)
                LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
            }
        }
        Thread(runnable).start()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                BACKUP_REQUEST_CODE -> onBackupFileSelected(data?.data)
                RESTORE_REQUEST_CODE -> onRestoreFileSelected(data?.data)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}
