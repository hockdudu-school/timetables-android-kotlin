package ch.hockdudu.timetables

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.view.MenuItem
import android.widget.TextView

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = getString(R.string.about_activity_name)

        findViewById<TextView>(R.id.source_code).movementMethod = LinkMovementMethod.getInstance()
        findViewById<TextView>(R.id.logo_license).movementMethod = LinkMovementMethod.getInstance()

        val authorsArray = resources.getStringArray(R.array.about_authors)
        val mergedAuthors = authorsArray.joinToString(", ")

        val authorsView = findViewById<TextView>(R.id.authors)
        authorsView.text = mergedAuthors
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }
}
