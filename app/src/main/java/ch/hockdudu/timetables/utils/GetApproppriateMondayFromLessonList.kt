package ch.hockdudu.timetables.utils

import ch.hockdudu.timetables.lesson.Lesson
import java.util.*

/**
 * Given a list with of [Lesson], returns the appropriate monday. The next week's monday is returned
 * when the last lesson is over, the current week's monday otherwise
 *
 * @return An instance of [Calendar] with the appropriate monday's date
 */
fun getAppropriateMondayFromLessonList(lessonList: List<Lesson>): Calendar {
    return getAppropriateMondayFromLessonList(lessonList, Calendar.getInstance().time)
}

/**
 * Given a list with of [Lesson], and the current [Date], return the appropriate monday. The next
 * week's monday is returned when the last lesson is over, the current week's monday otherwise
 *
 * @return An instance of [Calendar] with the appropriate monday's date
 */
fun getAppropriateMondayFromLessonList(lessonList: List<Lesson>, now: Date): Calendar {
    if (lessonList.isEmpty())
        return thisWeeksMonday()

    (lessonList as MutableList).sort()

    //If last lesson is over use next week's calendar, otherwise use this week's calendar
    return if (convertLessonTimeToDate(lessonList.last().end, lessonList.last().dayOfWeek, thisWeeksMonday()) < now) {
        nextWeeksMonday()
    } else {
        thisWeeksMonday()
    }
}