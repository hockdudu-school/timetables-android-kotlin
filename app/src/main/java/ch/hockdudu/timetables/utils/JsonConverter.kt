package ch.hockdudu.timetables.utils

import ch.hockdudu.timetables.lesson.Lesson
import ch.hockdudu.timetables.lesson.LessonTime
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.lang.NumberFormatException

/**
 * Given a list of [Lesson], returns a Json as String with them, separated by the day of week
 */
fun convertLessonsToJson(lessons: List<Lesson>): String {

    val separatedLessons = separateLessonByDate(lessons)

    val returnJsonObject = JSONObject()

    for ((dayOfWeek, lessonList) in separatedLessons) {

        val lessonsJsonArray = JSONArray()

        for (lesson in lessonList) {
            val lessonObject = JSONObject()

            lessonObject.put("start", lesson.start.toString())
            lessonObject.put("end", lesson.end.toString())
            lessonObject.put("summary", lesson.summary)
            lessonObject.put("teacher", lesson.teacher)
            lessonObject.put("location", lesson.location)

            lessonsJsonArray.put(lessonObject)
        }


        returnJsonObject.put(dayOfWeek.toString(), lessonsJsonArray)
    }

    return returnJsonObject.toString()
}

/**
 * Given a [String] with Json, return a list of [Lesson], or null if the Json couldn't be parsed
 */
fun convertJsonStringToLessonsList(jsonString: String): List<Lesson>? {
    try {
        val lessonList = mutableListOf<Lesson>()
        val jsonObject = JSONObject(jsonString)

        for (key in jsonObject.keys()) {

            val jsonArray = jsonObject.getJSONArray(key)

            for (i in 0 until jsonArray.length()) {
                val lessonJsonObject = jsonArray.getJSONObject(i)

                val lesson = Lesson()

                lesson.dayOfWeek = Integer.valueOf(key)
                lesson.start = LessonTime(lessonJsonObject.optString("start"))
                lesson.end = LessonTime(lessonJsonObject.optString("end"))
                lesson.summary = lessonJsonObject.optString("summary")
                lesson.teacher = lessonJsonObject.optString("teacher")
                lesson.location = lessonJsonObject.optString("location")

                lessonList += lesson
            }
        }

        return lessonList
    } catch (_: JSONException) {
        return null
    } catch (_: NumberFormatException) {
        return null
    }
}