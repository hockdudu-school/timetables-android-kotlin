package ch.hockdudu.timetables.utils

import ch.hockdudu.timetables.lesson.Lesson

/**
 * Separates a list of [Lesson] in a map of Int -> List of Lesson
 */
fun separateLessonByDate(lessonList: List<Lesson>): Map<Int, List<Lesson>> {
    val dateSeparatedLessons: MutableMap<Int, MutableList<Lesson>> = mutableMapOf()

    for (lesson in lessonList) {

        if (!dateSeparatedLessons.containsKey(lesson.dayOfWeek))
            dateSeparatedLessons[lesson.dayOfWeek] = mutableListOf()

        dateSeparatedLessons[lesson.dayOfWeek]!!.add(lesson)
    }

    return dateSeparatedLessons
}