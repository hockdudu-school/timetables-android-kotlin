package ch.hockdudu.timetables.utils

import ch.hockdudu.timetables.lesson.LessonTime
import java.util.*

/**
 * Given a [LessonTime], the lesson's day of week and an instance of [Calendar] with the wanted
 * week's monday, return a [Date] that corresponds to the given [LessonTime]
 */
fun convertLessonTimeToDate(time: LessonTime, dayOfWeek: Int, calendarWithWantedMonday: Calendar): Date {
    val localCalendarClone = calendarWithWantedMonday.clone() as Calendar
    while (localCalendarClone.get(Calendar.DAY_OF_WEEK) != dayOfWeek) {
        localCalendarClone.add(Calendar.DATE, 1)
    }

    localCalendarClone.set(Calendar.HOUR_OF_DAY, time.hour)
    localCalendarClone.set(Calendar.MINUTE, time.minute)
    localCalendarClone.set(Calendar.SECOND, 0)

    return localCalendarClone.time
}