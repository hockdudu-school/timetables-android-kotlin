package ch.hockdudu.timetables.utils

import java.util.*

fun thisWeeksMonday(): Calendar {
    val calendar = Calendar.getInstance()

    while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY)
        calendar.add(Calendar.DATE, -1)

    return calendar
}

fun nextWeeksMonday(): Calendar {
    val calendar = Calendar.getInstance()

    do calendar.add(Calendar.DATE, 1)
    while (calendar.get(Calendar.DAY_OF_WEEK) != Calendar.MONDAY)

    return calendar
}