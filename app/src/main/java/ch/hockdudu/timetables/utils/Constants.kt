package ch.hockdudu.timetables.utils

class Constants {
    companion object {

        const val EXTRA_LESSON = "ch.hockdudu.timetables.LESSON"

        const val ACTIVITY_RESULT_LESSON_UPDATE = 0

        const val INTENT_FILTER_REFRESH_ITEMS = "refreshItems"
        const val INTENT_FILTER_REFRESH_FROM_DB = "refreshItemsDB"

        const val SAVED_INSTANCE_SHOULD_RELOAD_LIST = "shouldReloadList"
    }
}