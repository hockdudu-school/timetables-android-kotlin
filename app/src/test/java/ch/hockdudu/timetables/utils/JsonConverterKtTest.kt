package ch.hockdudu.timetables.utils

import ch.hockdudu.timetables.lesson.Lesson
import ch.hockdudu.timetables.lesson.LessonTime
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import java.util.*

@RunWith(RobolectricTestRunner::class)
class JsonConverterKtTest {

    @Test
    fun jsonConvertInteroperability() {
        val lesson1 = Lesson()
        lesson1.summary = "SUMMARY"
        lesson1.dayOfWeek = 4
        lesson1.start = LessonTime("16:15")

        val lesson2 = Lesson()
        lesson2.teacher = "Mr. Someone"
        lesson2.dayOfWeek = 1
        lesson2.id = 2

        val lessonsList = Arrays.asList(lesson1, lesson2)

        val lessonsJson = convertLessonsToJson(lessonsList)

        val convertedLessonsList = convertJsonStringToLessonsList(lessonsJson)!!

        assertEquals(lesson1.summary, convertedLessonsList[0].summary)
        assertEquals(lesson1.dayOfWeek, convertedLessonsList[0].dayOfWeek)
        assertEquals(lesson1.start, convertedLessonsList[0].start)

        assertEquals(lesson2.teacher, convertedLessonsList[1].teacher)
        assertEquals(lesson2.dayOfWeek, convertedLessonsList[1].dayOfWeek)
        assertEquals(0, convertedLessonsList[1].id)
    }
}