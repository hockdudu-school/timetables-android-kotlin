package ch.hockdudu.timetables.utils

import ch.hockdudu.timetables.lesson.LessonTime
import org.junit.Test

import org.junit.Assert.*
import java.util.*

class ConvertLessonTimeToDateKtTest {

    @Test
    fun convertLessonTimeToDate() {
        val lessonTime = LessonTime("12:00")
        val dayOfWeek = 4 //Wednesday

        val calendarWithWantedMonday = Calendar.getInstance()
        calendarWithWantedMonday.set(2000, 0, 3, 0, 0, 0) //Monday
        calendarWithWantedMonday.set(Calendar.MILLISECOND, 0)

        val wantedCalendar = Calendar.getInstance()
        wantedCalendar.set(2000, 0, 5, 12, 0, 0) //Wednesday
        wantedCalendar.set(Calendar.MILLISECOND, 0)
        val wantedTime = wantedCalendar.time

        val convertedLessonTime = convertLessonTimeToDate(lessonTime, dayOfWeek, calendarWithWantedMonday)

        assertEquals(wantedTime, convertedLessonTime)
    }
}