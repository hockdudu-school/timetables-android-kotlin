package ch.hockdudu.timetables.utils

import ch.hockdudu.timetables.lesson.Lesson
import org.junit.Test

import org.junit.Assert.*
import java.util.*

class SeparateLessonsByDateKtTest {

    @Test
    fun separateLessonByDate() {
        val lesson1 = Lesson()
        lesson1.dayOfWeek = Calendar.MONDAY

        val lesson2 = Lesson()
        lesson2.dayOfWeek = Calendar.MONDAY

        val lesson3 = Lesson()
        lesson3.dayOfWeek = Calendar.TUESDAY

        val lessonList = Arrays.asList(lesson1, lesson2, lesson3)

        val expectedList = mutableMapOf<Int, List<Lesson>>()
        expectedList[Calendar.MONDAY] = Arrays.asList(lesson1, lesson2)
        expectedList[Calendar.TUESDAY] = Arrays.asList(lesson3)

        val returnList = separateLessonByDate(lessonList)

        assertEquals(expectedList, returnList)
    }
}