package ch.hockdudu.timetables.lesson

import org.junit.Assert.*
import org.junit.Test
import java.util.*

class LessonTest {

    @Test
    fun equality() {
        val lesson1 = Lesson()
        lesson1.teacher = "Someone"
        lesson1.summary = "Summary"
        lesson1.dayOfWeek = 2
        lesson1.start = LessonTime("02:20")

        val lesson2 = Lesson()
        lesson2.teacher = "Someone"
        lesson2.summary = "Summary"
        lesson2.dayOfWeek = 2
        lesson2.start = LessonTime("02:20")

        assertEquals(lesson1, lesson2)

        lesson2.dayOfWeek = 3
        assertNotEquals(lesson1, lesson2)
        lesson2.dayOfWeek = 2

        lesson2.location = ""
        assertEquals(lesson1, lesson2)

        assertEquals(lesson1.hashCode(), lesson2.hashCode())
    }

    @Test
    fun dayOfWeekCorrectness() {
        val lesson = Lesson()

        lesson.dayOfWeek = Calendar.WEDNESDAY
        assertEquals(Calendar.WEDNESDAY, lesson.dayOfWeek)

        lesson.dayOfWeek = 3 // Tuesday
        assertEquals(Calendar.TUESDAY, lesson.dayOfWeek)

        lesson.dayOfWeek = 22 // Invalid
        assertEquals(Lesson().dayOfWeek, lesson.dayOfWeek) // Default should be set
    }
}