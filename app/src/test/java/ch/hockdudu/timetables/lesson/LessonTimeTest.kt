package ch.hockdudu.timetables.lesson

import org.junit.Assert.*
import org.junit.Test

class LessonTimeTest {

    @Test
    fun constructors() {
        val lessonTime1 = LessonTime()
        lessonTime1.hour = 16
        lessonTime1.minute = 15

        val lessonTime2 = LessonTime(16 * 60 + 15)

        val lessonTime3 = LessonTime("16:15")

        assertEquals(lessonTime1, lessonTime2)
        assertEquals(lessonTime2, lessonTime3)
    }

    @Test
    fun invalidTimeCorrect() {
        val lessonTime = LessonTime()

        lessonTime.hour = 22
        assertEquals(22, lessonTime.hour)

        lessonTime.hour = 74
        assertEquals(0, lessonTime.hour)

        lessonTime.hour = -1
        assertEquals(0, lessonTime.hour)

        lessonTime.minute = 12
        assertEquals(12, lessonTime.minute)

        lessonTime.minute = 80
        assertEquals(0, lessonTime.minute)

        lessonTime.minute = -1
        assertEquals(0, lessonTime.minute)
    }

    @Test
    fun comparison() {
        val lessonTime1 = LessonTime("14:22")
        val lessonTime2 = LessonTime("15:22")
        val lessonTime3 = LessonTime("14:50")
        val lessonTime4 = LessonTime("14:22")
        val lessonTime5 = LessonTime("13:22")

        assertEquals(-1, lessonTime1.compareTo(lessonTime2))
        assertEquals(-1, lessonTime1.compareTo(lessonTime3))
        assertEquals(0, lessonTime1.compareTo(lessonTime4))
        assertEquals(1, lessonTime1.compareTo(lessonTime5))
    }
}